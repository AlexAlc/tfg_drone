/**
 *  Code based on https://github.com/kriswiner/MPU6050
 *  @author kriswiner
 *  Modified:
 *    16-11-2018 - AlexAlc
 **/

#ifndef MPU6050_H
#define MPU6050_H
 
#include "mbed.h"

// Set initial input parameters
enum Ascale {
  AFS_2G = 0,
  AFS_4G,
  AFS_8G,
  AFS_16G
};

enum Gscale {
  GFS_250DPS = 0,
  GFS_500DPS,
  GFS_1000DPS,
  GFS_2000DPS
};

class MPU6050
{
private:
  //Set up I2C, (SDA,SCL)
  I2C i2c;
  //Timer for integration time calculation
  Timer t;
    
  // Specify sensor full scale
  int Gscale;
  int Ascale;
  float aRes, gRes; // scale resolutions per LSB for the sensors
    
  // Pin definitions
  //int intPin = 12;  // These can be changed, 2 and 3 are the Arduinos ext int pins

  int16_t accelCount[3];  // Stores the 16-bit signed accelerometer sensor output
  float ax, ay, az;       // Stores the real accel value in g's
  int16_t gyroCount[3];   // Stores the 16-bit signed gyro sensor output
  float gx, gy, gz;       // Stores the real gyro value in degrees per seconds
  float gyroBias[3], accelBias[3]; // Bias corrections for gyro and accelerometer
  int16_t tempCount;   // Stores the real internal chip temperature in degrees Celsius
  float temperature;
  float SelfTest[6];

  // parameters for 6 DoF sensor fusion calculations
  float beta, zeta;
  float pitch, yaw, roll;
  float deltat;                              // integration interval for both filter schemes
  int lastUpdate, firstUpdate, Now;     // used to calculate integration interval                               // used to calculate integration interval
  float q[4];                                       // vector to hold quaternion
  float PI;
public:
  MPU6050(PinName sda,PinName scl);
  ~MPU6050() { }

  //Power-up/down
  void initMPU6050();
  void resetMPU6050();
  uint8_t CheckConnection();
  int DataInterrupt();

  //Gyro/Accel/Temp readings
  void update_values();
  float getGres();
  float getAres();
  void readAccelData();
  void readGyroData();
  int16_t readTempData();
  inline float Yaw(){return yaw;}
  inline float Pitch(){return pitch;}
  inline float Roll(){return roll;}
  
  inline float Gx(){return gx;}
  inline float Gy(){return gy;}
  inline float Gz(){return gz;}

  //Features
  void LowPowerAccelOnly();
  void calibrateMPU6050();
  int MPU6050SelfTest();
  void MadgwickQuaternionUpdate();
  void YawPitchRoll();

  //Low-level communication
  void writeByte(uint8_t address, uint8_t subAddress, uint8_t data);
  char readByte(uint8_t address, uint8_t subAddress);
  void readBytes(uint8_t address, uint8_t subAddress, uint8_t count, uint8_t * dest);
};
#endif