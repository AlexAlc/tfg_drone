#ifndef _PROJECT_CONF_H_
#define _PROJECT_CONF_H_

#include "mbed.h"
#include "MPU6050.h"
#include "nRF24L01P.h"

#ifdef GLOWVE_RECEIVER
    #include "PID_mbed.h"
#endif

#define SERIAL_DEBUG
//#define OSCILOSCOPE

#define SERIAL_BAUDRATE     115200

#define saturation(value,max,min)   (value > max ? max : (value < min ? min : value))

//--------------------------------PROBE_PINS--------------------------------
//Osciloscope pin
#define OSCILOSCOPE_SET     
#define OSCILOSCOPE_PORT    
#define OSCILOSCOPE_PIN     

//--------------------------------THROTTLE--------------------------------
#define THROTTLE_PIN    A0

#define ThrottleToValue(n)   ((n*RANGE_VALUE)+MIN_VALUE)
#define ThrottleSaturation(value)     saturation(value, MAX_VALUE, MIN_VALUE)

// Values for THROTTLE same as MOTORS values (MIN_VALUE, MAX_VALUE, RANGE_VALUE...)

//--------------------------------MOTORS--------------------------------
#define MOTOR_UL    D2
#define MOTOR_UR    D6
#define MOTOR_DL    A2
#define MOTOR_DR    A3

#define PERIOD      0.020       // 20ms as servos
#define MAX_PULSE   0.002       // 2ms max
#define MAX_VALUE   100
#define MIN_PULSE   0.001       // 1ms min
#define MIN_VALUE   0
#define RUN_VALUE   8          // Min value for motors to start running
#define RANGE_PULSE (MAX_PULSE - MIN_PULSE)
#define RANGE_VALUE (MAX_VALUE - MIN_VALUE)

#define ValueToPulse(n)   ((n-MIN_VALUE)*(RANGE_PULSE/RANGE_VALUE))+MIN_PULSE
#define PulseToValue(n)   ((n-MIN_PULSE)*(RANGE_VALUE/RANGE_PULSE))+MIN_VALUE

#define MotorSaturation(value)     saturation(value, MAX_VALUE, RUN_VALUE)

#define ESC_WAIT    3

//--------------------------------PID--------------------------------
#define PID_PITCH_PROPORTIONAL  0.15000000
#define PID_PITCH_INTEGRAL      0.00000000
#define PID_PITCH_DERIVATIVE    0.00005000

#define PID_ROLL_PROPORTIONAL   PID_PITCH_PROPORTIONAL
#define PID_ROLL_INTEGRAL       PID_PITCH_INTEGRAL
#define PID_ROLL_DERIVATIVE     PID_PITCH_DERIVATIVE

#define PID_YAW_PROPORTIONAL    0.05000000
#define PID_YAW_INTEGRAL        0.00000010
#define PID_YAW_DERIVATIVE      0.00000000

#define PID_OUT_PITCH_LIM_LOW   -50
#define PID_OUT_PITCH_LIM_UP     50

#define PID_OUT_ROLL_LIM_LOW    -50
#define PID_OUT_ROLL_LIM_UP      50

#define PID_OUT_YAW_LIM_LOW     -50
#define PID_OUT_YAW_LIM_UP       50

//--------------------------------MPU--------------------------------
// {0x68 , 0x69}
#define MPU_DIR             ((uint8_t)0x68)

//--------------------------------RADIO--------------------------------
#define RADIO_PIPE          ((uint64_t)0xE8E8F0F0E1LL)
#define RADIO_PIPE_DEFAULT  ((uint64_t)0xE7E7E7E7E7LL)
#define RADIO_CE_PIN        9
#define RADIO_CSN_PIN       10
// {RF24_250KBPS , RF24_1MBPS , RF24_2MBPS}
#define RADIO_DATA_RATE     RF24_2MBPS

//User parameters
#define POWER_INITIAL       30
#define POWER_THRESHOLD     20
#define ANGLE_INITIAL_X     0
#define ANGLE_INITIAL_Y     0
#define ANGLE_INITIAL_Z     0

// uint8_t sent_data[13]={0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
uint8_t sent_data[32] = { 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                          0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                          0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                          0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// struct Data_to_be_sent {
//   float anglex = 0;
//   float angley = 0;
//   float anglez = 0;
//   byte joyY = 0;
// };
// Data_to_be_sent sent_data;

#ifdef SERIAL_DEBUG
    #define IFSERIAL(x)     {x;}
#else
    #define IFSERIAL(x)
#endif

#endif //_PROJECT_CONF_H_
