#define GLOWVE_EMITTER

#include "project_conf.h"


AnalogIn   throttle_pot(THROTTLE_PIN);
MPU6050 mpu6050(I2C_SDA, I2C_SCL);
nRF24L01P my_nrf24l01p(D11, D12, D13, D10, D9, D8);    // mosi, miso, sck, csn, ce, irq
Serial pc(USBTX, USBRX);

Timer stable_read;
int stable_read_flag = 0;

float throttle;
float yaw = 0, pitch = 0, roll = 0;
float syaw = 0, spitch = 0, sroll = 0;
float prev_yaw = 0, prev_pitch = 0, prev_roll = 0;
char txData[GOWVE_TRANSFERSIZE], rxData[GOWVE_TRANSFERSIZE];

int main()
{
    pc.baud(115200);

    uint8_t whoami = mpu6050.CheckConnection();
    pc.printf("I AM 0x%x\n\r", whoami);

    if (whoami == 0x68)
    {
        pc.printf("MPU6050 ready");
        wait(1);
        int result = mpu6050.MPU6050SelfTest();
        
        if(result)
        {
            mpu6050.resetMPU6050();
            mpu6050.calibrateMPU6050();
            mpu6050.initMPU6050();
            wait(2);
        }
        else
        {
            pc.printf("Error self-test\n\r"); 
        }
    }
    else
    {
        pc.printf("Could not connect to MPU6050: \n\r");
        pc.printf("%#x \n",  whoami);
 
        while(1) ;
    }  
        
    my_nrf24l01p.InitialSetup();

    // Display the setup of the nRF24L01+ chip
    pc.printf( "nRF24L01+ Frequency    : %d MHz\r\n",  my_nrf24l01p.getRfFrequency() );
    pc.printf( "nRF24L01+ Output power : %d dBm\r\n",  my_nrf24l01p.getRfOutputPower() );
    pc.printf( "nRF24L01+ Data Rate    : %d kbps\r\n", my_nrf24l01p.getAirDataRate() );
    pc.printf( "nRF24L01+ TX Address   : 0x%010llX\r\n", my_nrf24l01p.getTxAddress() );
    pc.printf( "nRF24L01+ RX Address   : 0x%010llX\r\n", my_nrf24l01p.getRxAddress() );
    wait(5);

    stable_read.start();
    
    while(1)
    {
        while(stable_read_flag == 0)
        {
            if(stable_read.read() > 15) 
            {
                stable_read_flag = 1;
                stable_read.stop();
                stable_read.reset();
            }
        }
            
        if(mpu6050.DataInterrupt()) 
        {
            mpu6050.update_values();
        }
        
        mpu6050.MadgwickQuaternionUpdate();

        //pc.printf("%f,%f,%f\n", mpu6050.Yaw(), mpu6050.Pitch(), mpu6050.Roll());

        throttle = (int16_t) ThrottleToValue(throttle_pot.read());

        //pc.printf("%f,%f,%f,%f\n",  throttle,mpu6050.Yaw(), mpu6050.Pitch(), mpu6050.Roll());
        
        yaw     = yaw * 0.8 + mpu6050.Yaw() * 0.2;
        yaw = saturation(yaw, 150, -150);
        //yaw = (yaw > -5 && yaw < 5) ? 0 : yaw;
        pitch   = pitch * 0.8 + mpu6050.Pitch() * 0.2;
        pitch = saturation(pitch, 150, -150);
        //pitch = (pitch > -5 && pitch < 5) ? 0 : pitch;
        roll    = roll * 0.8 + mpu6050.Roll() * 0.2;
        roll = saturation(roll, 150, -150);
        //roll = (roll > -5 && roll < 5) ? 0 : roll;
        
        //yaw = 0; pitch = 0; roll = 0;
        
        txData[0] = (char)((int16_t)yaw >> 8);
        txData[1] = (char)((int16_t)yaw);
        txData[2] = (char)((int16_t)pitch >> 8);
        txData[3] = (char)((int16_t)pitch);
        txData[4] = (char)((int16_t)roll >> 8);
        txData[5] = (char)((int16_t)roll);
        
        txData[6] = (char)((int16_t)throttle >> 8);
        txData[7] = (char)((int16_t)throttle);
        
        my_nrf24l01p.write( NRF24L01P_PIPE_P0, txData, GOWVE_TRANSFERSIZE );
        
        
        float dif_yaw = yaw - prev_yaw;
        prev_yaw = yaw;
        float dif_pitch = pitch - prev_pitch;
        prev_pitch = pitch;
        float dif_roll = roll - prev_roll;
        prev_roll = roll;
        
        pc.printf("%f,%f,%f,%f\n", yaw, pitch, roll, throttle);
        // pc.printf("%f,%f,%f\n", mpu6050.Gx(), mpu6050.Gy(), mpu6050.Gz());
    }
}
