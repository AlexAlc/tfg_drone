#ifndef _CONF_PINS_H_
#define _CONF_PINS_H_

#include "project_conf.h"

//--------------------------------PROBE-----------------------------------

#define OSCILOSCOPE_SET     
#define OSCILOSCOPE_PORT    
#define OSCILOSCOPE_PIN     

//--------------------------------THROTTLE--------------------------------

#define THROTTLE_PIN    A0

//--------------------------------MOTORS----------------------------------

#define MOTOR_UL        D2
#define MOTOR_UR        D6
#define MOTOR_DL        A2
#define MOTOR_DR        A3

//--------------------------------MPU-------------------------------------

#define MPU_SDA_PIN     I2C_SDA
#define MPU_SCL_PIN     I2C_SCL

//--------------------------------RADIO-----------------------------------

#define NRF24_MOSI      D11
#define NRF24_MISO      D12
#define NRF24_SCK       D13
// #define NRF24_CSN       D10
// #define NRF24_CE        D9
#define RADIO_CE_PIN    9
#define RADIO_CSN_PIN   10
#define NRF24_IRQ       D8

#endif //_CONF_PINS_H_
