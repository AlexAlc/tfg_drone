#ifndef _CONF_VALUES_H_
#define _CONF_VALUES_H_

#include "project_conf.h"

//--------------------------------MOTORS----------------------------------

#define PERIOD                  0.020       // 20ms as servos
#define MAX_PULSE               0.002       // 2ms max
#define MIN_PULSE               0.001       // 1ms min
#define MAX_VALUE               100
#define MIN_VALUE               0
#define RUN_VALUE               8          // Min value for motors to start running

#define ESC_WAIT                3           //in secs

//--------------------------------PID-------------------------------------

#define PID_PITCH_PROPORTIONAL  10.0         // 5.0
#define PID_PITCH_INTEGRAL      5.0         // 5.0
#define PID_PITCH_DERIVATIVE    0.0         //0.000008

#define PROLL_RATIO             0.95

#define PID_ROLL_PROPORTIONAL   PID_PITCH_PROPORTIONAL * PROLL_RATIO
#define PID_ROLL_INTEGRAL       PID_PITCH_INTEGRAL * PROLL_RATIO
#define PID_ROLL_DERIVATIVE     PID_PITCH_DERIVATIVE * PROLL_RATIO

#define PID_YAW_PROPORTIONAL    0.5     // 0.2
#define PID_YAW_INTEGRAL        15.0    // 30
#define PID_YAW_DERIVATIVE      0.0

//--------------------------------PID GYRO---------------------------------

#define PID_PITCH_PROPORTIONAL_GYRO  0.05
#define PID_PITCH_INTEGRAL_GYRO      5.0
#define PID_PITCH_DERIVATIVE_GYRO    0.000015

#define PID_ROLL_PROPORTIONAL_GYRO   PID_PITCH_PROPORTIONAL_GYRO * PROLL_RATIO
#define PID_ROLL_INTEGRAL_GYRO       PID_PITCH_INTEGRAL_GYRO * PROLL_RATIO
#define PID_ROLL_DERIVATIVE_GYRO     PID_PITCH_DERIVATIVE_GYRO * PROLL_RATIO

#define PID_YAW_PROPORTIONAL_GYRO    0.1
#define PID_YAW_INTEGRAL_GYRO        15.0
#define PID_YAW_DERIVATIVE_GYRO      0.0

#define PID_OUT_PITCH_LIM_LOW   -40
#define PID_OUT_PITCH_LIM_UP     40

#define PID_OUT_ROLL_LIM_LOW    -40
#define PID_OUT_ROLL_LIM_UP      40

#define PID_OUT_YAW_LIM_LOW     -40
#define PID_OUT_YAW_LIM_UP       40

#define POWER_INITIAL       30
#define POWER_THRESHOLD     20
#define ANGLE_INITIAL_X     0
#define ANGLE_INITIAL_Y     0
#define ANGLE_INITIAL_Z     0

//--------------------------------MPU-------------------------------------

#define MPU_DIR                 ((uint8_t)0x68)     // {0x68 , 0x69}

//--------------------------------RADIO-----------------------------------

#define RADIO_PIPE          ((uint64_t)0xE8E8F0F0E1LL)
#define RADIO_PIPE_DEFAULT  ((uint64_t)0xE7E7E7E7E7LL)

#define RADIO_DATA_RATE     RF24_2MBPS          // {RF24_250KBPS , RF24_1MBPS , RF24_2MBPS}

uint8_t sent_data[32] = { 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                          0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                          0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                          0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// struct Data_to_be_sent {
//   float anglex = 0;
//   float angley = 0;
//   float anglez = 0;
//   byte joyY = 0;
// };
// Data_to_be_sent sent_data;

#endif //_CONF_VALUES_H_