#ifndef _CONF_MACROS_H_
#define _CONF_MACROS_H_

#include "project_conf.h"
#include "conf_values.h"

#define saturation(value,max,min)       (value > max ? max : (value < min ? min : value))

//--------------------------------THROTTLE--------------------------------

// Values for THROTTLE same as MOTORS values (MIN_VALUE, MAX_VALUE, RANGE_VALUE...)

#define ThrottleToValue(n)              ((n*RANGE_VALUE)+MIN_VALUE)
#define ThrottleSaturation(value)       saturation(value, MAX_VALUE, MIN_VALUE)

//--------------------------------MOTORS----------------------------------

#define ValueToPulse(n)                 ((n-MIN_VALUE)*(RANGE_PULSE/RANGE_VALUE))+MIN_PULSE
#define PulseToValue(n)                 ((n-MIN_PULSE)*(RANGE_VALUE/RANGE_PULSE))+MIN_VALUE

#define MotorSaturation(value)          saturation(value, MAX_VALUE, RUN_VALUE)

#define RANGE_PULSE (MAX_PULSE - MIN_PULSE)
#define RANGE_VALUE (MAX_VALUE - MIN_VALUE)

//--------------------------------DEV_OPTIONS-----------------------------

#define SERIAL_DEBUG
//#define OSCILOSCOPE

#define SERIAL_BAUDRATE     115200

#ifdef SERIAL_DEBUG
    #define IFSERIAL(x)     {x;}
#else
    #define IFSERIAL(x)
#endif

#endif //_CONF_MACROS_H_
