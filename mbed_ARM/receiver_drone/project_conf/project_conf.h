#ifndef _PROJECT_CONF_H_
#define _PROJECT_CONF_H_

    #include "mbed.h"
    #include "MPU6050.h"
    #include "nRF24L01P.h"

#ifdef GLOWVE_RECEIVER
    #include "PID_mbed.h"
#endif

#include "conf_pins.h"
#include "conf_values.h"
#include "conf_macros.h"

#endif //_PROJECT_CONF_H_

