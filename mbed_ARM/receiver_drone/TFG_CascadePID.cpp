#define GLOWVE_RECEIVER

#include "project_conf.h"

//--------------------------------MOTORS--------------------------------
PwmOut UL_motor(MOTOR_UL);
PwmOut UR_motor(MOTOR_UR);
PwmOut DL_motor(MOTOR_DL);
PwmOut DR_motor(MOTOR_DR);
float UL_out, UR_out, DL_out, DR_out;

//--------------------------------MPU--------------------------------
MPU6050 mpu6050(I2C_SDA, I2C_SCL);
int yaw, pitch, roll;
float gx = 0, gy = 0, gz = 0;

//--------------------------------RADIO--------------------------------
nRF24L01P my_nrf24l01p(D11, D12, D13, D10, D9, D8);    // mosi, miso, sck, csn, ce, irq
char txData[GOWVE_TRANSFERSIZE], rxData[GOWVE_TRANSFERSIZE];

//--------------------------------PID--------------------------------
double throttle = 0;
double setpoint_yaw, input_yaw, output_yaw;
double setpoint_pitch, input_pitch, output_pitch;
double setpoint_roll, input_roll, output_roll;

PID PID_yaw(&input_yaw, &output_yaw, &setpoint_yaw, 
            PID_YAW_PROPORTIONAL, PID_YAW_INTEGRAL, PID_YAW_DERIVATIVE, DIRECT);
PID PID_pitch(&input_pitch, &output_pitch, &setpoint_pitch,
            PID_PITCH_PROPORTIONAL, PID_PITCH_INTEGRAL, PID_PITCH_DERIVATIVE, DIRECT);
PID PID_roll(&input_roll, &output_roll, &setpoint_roll, 
            PID_ROLL_PROPORTIONAL, PID_ROLL_INTEGRAL, PID_ROLL_DERIVATIVE, DIRECT);

//--------------------------------PID Gyro-------------------------
double setpoint_yaw_gyro, input_yaw_gyro, output_yaw_gyro;
double setpoint_pitch_gyro, input_pitch_gyro, output_pitch_gyro;
double setpoint_roll_gyro, input_roll_gyro, output_roll_gyro;

PID PID_yaw_gyro(&input_yaw_gyro, &output_yaw_gyro, &setpoint_yaw_gyro, 
            PID_YAW_PROPORTIONAL_GYRO, PID_YAW_INTEGRAL_GYRO, PID_YAW_DERIVATIVE_GYRO, DIRECT);
PID PID_pitch_gyro(&input_pitch_gyro, &output_pitch_gyro, &setpoint_pitch_gyro,
            PID_PITCH_PROPORTIONAL_GYRO, PID_PITCH_INTEGRAL_GYRO, PID_PITCH_DERIVATIVE_GYRO, DIRECT);
PID PID_roll_gyro(&input_roll_gyro, &output_roll_gyro, &setpoint_roll_gyro, 
            PID_ROLL_PROPORTIONAL_GYRO, PID_ROLL_INTEGRAL_GYRO, PID_ROLL_DERIVATIVE_GYRO, DIRECT);

Serial pc(USBTX, USBRX);
Timer stable_read;
int stable_read_flag = 0;

int main()
{
    IFSERIAL( pc.baud(115200));

    UL_motor.period(PERIOD);
    UR_motor.period(PERIOD);
    DL_motor.period(PERIOD);
    DR_motor.period(PERIOD);
    UL_motor.pulsewidth(MAX_PULSE);
    UR_motor.pulsewidth(MAX_PULSE);
    DL_motor.pulsewidth(MAX_PULSE);
    DR_motor.pulsewidth(MAX_PULSE);
    wait(ESC_WAIT);
    UL_motor.pulsewidth(MIN_PULSE);
    UR_motor.pulsewidth(MIN_PULSE);
    DL_motor.pulsewidth(MIN_PULSE);
    DR_motor.pulsewidth(MIN_PULSE);
    wait(ESC_WAIT);
    
    uint8_t whoami = mpu6050.CheckConnection();
    IFSERIAL( pc.printf("I AM 0x%x\n\r", whoami));

    if (whoami == 0x68)
    {
        IFSERIAL( pc.printf("MPU6050 ready"));
        int result = mpu6050.MPU6050SelfTest();
        
        if(result)
        {
            mpu6050.resetMPU6050();
            mpu6050.calibrateMPU6050();
            mpu6050.initMPU6050();
            wait(2);
        }
        else
        {
            IFSERIAL( pc.printf("Error self-test\n\r")); 
        }
    }
    else
    {
        IFSERIAL( pc.printf("Could not connect to MPU6050: \n\r"));
        IFSERIAL( pc.printf("%#x \n",  whoami));
 
        while(1) ;
    }  

    PID_pitch.SetOutputLimits(-160, 160);
    PID_roll.SetOutputLimits(-160, 160);
    PID_yaw.SetOutputLimits(-160, 160);
    PID_yaw.SetSampleTime(10);
    PID_pitch.SetSampleTime(10);
    PID_roll.SetSampleTime(10); 

    PID_pitch_gyro.SetOutputLimits(-20, 20);
    PID_roll_gyro.SetOutputLimits(-20, 20);
    PID_yaw_gyro.SetOutputLimits(-20, 20);
    PID_yaw_gyro.SetSampleTime(10);
    PID_pitch_gyro.SetSampleTime(10);
    PID_roll_gyro.SetSampleTime(10); 

    my_nrf24l01p.InitialSetup();
    my_nrf24l01p.setReceiveMode();

    // Display the setup of the nRF24L01+ chip
    IFSERIAL( pc.printf( "nRF24L01+ Frequency    : %d MHz\r\n",  my_nrf24l01p.getRfFrequency() ));
    IFSERIAL( pc.printf( "nRF24L01+ Output power : %d dBm\r\n",  my_nrf24l01p.getRfOutputPower() ));
    IFSERIAL( pc.printf( "nRF24L01+ Data Rate    : %d kbps\r\n", my_nrf24l01p.getAirDataRate() ));
    IFSERIAL( pc.printf( "nRF24L01+ TX Address   : 0x%010llX\r\n", my_nrf24l01p.getTxAddress() ));
    IFSERIAL( pc.printf( "nRF24L01+ RX Address   : 0x%010llX\r\n", my_nrf24l01p.getRxAddress() ));
    
    UL_motor.pulsewidth(ValueToPulse(RUN_VALUE));
    UR_motor.pulsewidth(ValueToPulse(RUN_VALUE));
    DL_motor.pulsewidth(ValueToPulse(RUN_VALUE));
    DR_motor.pulsewidth(ValueToPulse(RUN_VALUE));
    wait(1);
    
    stable_read.start();

    while(1)
    {
        if(mpu6050.DataInterrupt()) 
        {
            mpu6050.update_values();
        }
        
        mpu6050.MadgwickQuaternionUpdate();

        gx = gx * 0.0 + mpu6050.Gx() * 1;
        gy = gy * 0.0 + mpu6050.Gy() * 1;
        gz = gz * 0.0 + mpu6050.Gz() * 1;

        // IFSERIAL( pc.printf("%f,%f,%f\n", mpu6050.Yaw(), mpu6050.Pitch(), mpu6050.Roll()));
        
        if(my_nrf24l01p.readable()){
            my_nrf24l01p.read(NRF24L01P_PIPE_P0, rxData, GOWVE_TRANSFERSIZE);
        }

        yaw     = (int16_t) ((rxData[0]<<8) | rxData[1]);
        pitch   = (int16_t) ((rxData[2]<<8) | rxData[3]);
        roll    = (int16_t) ((rxData[4]<<8) | rxData[5]);

        throttle    = (int16_t) ((rxData[6]<<8) | rxData[7]);
        
        if(stable_read.read() > 15) 
        {
            stable_read_flag = 1;
            stable_read.stop();
            stable_read.reset();
            for(int i=0; i < throttle; i++)
            {
                UL_motor.pulsewidth(ValueToPulse(i));
                UR_motor.pulsewidth(ValueToPulse(i));
                DL_motor.pulsewidth(ValueToPulse(i)); 
                DR_motor.pulsewidth(ValueToPulse(i));
                wait(0.05);
            }
        }
        // IFSERIAL( pc.printf("%d,%d,%d\n", yaw, pitch, roll));
        // if(angz > POWER_THRESHOLD)
        if(stable_read_flag && throttle > 10)
        {
            PID_pitch.SetMode(AUTOMATIC);
            PID_roll.SetMode(AUTOMATIC);
            PID_yaw.SetMode(AUTOMATIC);
            input_yaw   = mpu6050.Yaw();
            input_pitch = mpu6050.Roll();
            input_roll  = mpu6050.Pitch();
            setpoint_yaw   = mpu6050.Yaw(); //yaw;
            setpoint_pitch = roll*0.2;
            setpoint_roll  = pitch*0.2;
            PID_yaw.Compute();
            PID_pitch.Compute();
            PID_roll.Compute();

            PID_pitch_gyro.SetMode(AUTOMATIC);
            PID_roll_gyro.SetMode(AUTOMATIC);
            PID_yaw_gyro.SetMode(AUTOMATIC);
            input_yaw_gyro   = gz;
            input_pitch_gyro = gx;
            input_roll_gyro  = gy;
            setpoint_yaw_gyro   = output_yaw ; // = yaw;
            setpoint_pitch_gyro = output_pitch ; // = pitch;
            setpoint_roll_gyro  = output_roll ; // = roll;
            PID_yaw_gyro.Compute();
            PID_pitch_gyro.Compute();
            PID_roll_gyro.Compute();

            // Thrust Angle Correction Factor
            /*if(throttle > 50 && throttle < 60){
                double tacf = 1 / (cos(mpu6050.Pitch()) * cos(mpu6050.Roll()));
                throttle *= tacf;
            }*/
            
            //throttle = saturation(yaw, MAX_VALUE, MIN_VALUE);
            throttle = ThrottleSaturation(throttle);

            float pitchf = 0; // pitch/100.0;
            float rollf = 0; // roll/100.0;
            float yawf = 0; // yaw/100.0;
            
            UL_out = throttle + output_pitch_gyro + output_roll_gyro + output_yaw_gyro + pitchf + rollf + yawf ; //(rear-right - CW)
            UR_out = throttle + output_pitch_gyro - output_roll_gyro - output_yaw_gyro + pitchf - rollf - yawf ; //(front-right - CCW)
            DL_out = throttle - output_pitch_gyro + output_roll_gyro - output_yaw_gyro - pitchf + rollf - yawf ; //(rear-left - CCW)
            DR_out = throttle - output_pitch_gyro - output_roll_gyro + output_yaw_gyro - pitchf - rollf + yawf ; //(front-left - CW)
            
            // if(UL_out > 100 || UR_out > 100 || DL_out > 100 || DL_out > 100){
            //     
            //  }            
            
            UL_out = MotorSaturation(UL_out);
            UR_out = MotorSaturation(UR_out);
            DL_out = MotorSaturation(DL_out);
            DR_out = MotorSaturation(DR_out);
        }
        else
        {
            UL_out = 0;
            UR_out = 0;
            DL_out = 0;
            DR_out = 0;

            //setpoint_yaw   = 0;
            //setpoint_pitch = 0;
            //setpoint_roll  = 0;
            PID_pitch.SetMode(MANUAL);
            PID_roll.SetMode(MANUAL);
            PID_yaw.SetMode(MANUAL);

            //setpoint_yaw_gyro   = 0;
            //setpoint_pitch_gyro = 0;
            //setpoint_roll_gyro  = 0;
            PID_pitch_gyro.SetMode(MANUAL);
            PID_roll_gyro.SetMode(MANUAL);
            PID_yaw_gyro.SetMode(MANUAL);
        }

        UL_motor.pulsewidth(ValueToPulse(UL_out));
        UR_motor.pulsewidth(ValueToPulse(UR_out));
        DL_motor.pulsewidth(ValueToPulse(DL_out)); 
        DR_motor.pulsewidth(ValueToPulse(DR_out)); 

        IFSERIAL( pc.printf("%d,%d,%d,%f\n", yaw, roll, pitch, throttle));
        // IFSERIAL( pc.printf("%f,%f,%f\n", mpu6050.Yaw(),mpu6050.Pitch(),mpu6050.Roll()));
        // IFSERIAL( pc.printf("%f,%f,%f,%f\n", UL_out, UR_out, DL_out, DR_out));
        // IFSERIAL( pc.printf("%f,%f,%f\n",output_yaw, output_pitch, output_roll));
    }
}
