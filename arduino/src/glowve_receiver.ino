#define GLOWVE_RECEIVER

#include "project_conf.h"

Servo leftmotor, rightmotor;

MPU_minimal mpu_drone(MPU_DIR);

//Define Variables we'll be connecting to
double setpoint, input, output;

//Specify the links and initial tuning parameters
double Kp = PID_PROPORTIONAL;
double Ki = PID_INTEGRAL;
double Kd = PID_DERIVATIVE;

PID PID_drone(&input, &output, &setpoint, Kp, Ki, Kd, DIRECT);

// NRF24
//const uint64_t my_radio_pipe = RADIO_PIPE;
RF24 radio(RADIO_CE_PIN, RADIO_CSN_PIN);

void setup()
{
    #ifdef SERIAL_DEBUG
        Serial.begin(SERIAL_BAUDRATE);
    #endif
    
    #ifdef OSCILOSCOPE
        OSCILOSCOPE_SET |= _BV(OSCILOSCOPE_PIN);
    #endif

    leftmotor.attach(MOTOR_PIN_LEFT);
    rightmotor.attach(MOTOR_PIN_RIGHT);
    leftmotor.write(0);
    rightmotor.write(0);
    delay(ESC_WAIT);

  mpu_drone.init();
  PID_drone.SetOutputLimits(PID_OUT_LIM_LOW, PID_OUT_LIM_UP);

    //Once again, begin and radio configuration
    radio.begin();
    radio.setAutoAck(false);
    radio.setDataRate(RADIO_DATA_RATE);
    radio.openReadingPipe(1,RADIO_PIPE);

    //We start the radio comunication
    radio.startListening();
}
void loop()
{
    #ifdef OSCILOSCOPE
        OSCILOSCOPE_PORT |= _BV(OSCILOSCOPE_PIN);
    #endif

    mpu_drone.update();

    if(sent_data.joyY > POWER_THRESHOLD)
    {
        PID_drone.SetMode(AUTOMATIC);
        input = mpu_drone.AngleX();
        setpoint = sent_data.angley;
        PID_drone.Compute();

        rightmotor.write(sent_data.joyY + output);
        leftmotor.write(sent_data.joyY - output);
    }
    else
    {
        rightmotor.write(0);
        leftmotor.write(0);
        setpoint=0;
        PID_drone.SetMode(MANUAL);
    }
    
    #ifdef OSCILOSCOPE
        OSCILOSCOPE_PORT &= !_BV(OSCILOSCOPE_PIN);
    #endif

    while ( radio.available() ) 
    {
        radio.read(&sent_data, sizeof(sent_data));
    }

    #ifdef SERIAL_DEBUG
        Serial.print(sent_data.anglex);
        Serial.print(",");
        Serial.print(sent_data.angley);

//        Serial.print(mpu_drone.anglex);
//        Serial.print(",");
//        Serial.print(mpu_drone.angley);
//        Serial.print(",");
//        Serial.print(mpu_drone.anglez);
//        Serial.print(",");
//        Serial.println(mpu_drone.joyY);
    #endif
}
