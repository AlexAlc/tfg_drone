#define GLOWVE_EMITTER

#include "project_conf.h"

MPU_minimal mpu_drone(MPU_DIR);

//const uint64_t my_radio_pipe = 0xE8E8F0F0E1LL;
RF24 radio(RADIO_CE_PIN, RADIO_CSN_PIN);

void setup()
{    
    #ifdef SERIAL_DEBUG
        Serial.begin(SERIAL_BAUDRATE);
    #endif  
    
    mpu_drone.init();

    radio.begin();
    radio.setAutoAck(false);
    radio.setDataRate(RADIO_DATA_RATE);
    radio.openWritingPipe(RADIO_PIPE);

    sent_data.joyY = POWER_INITIAL;
}

void loop()
{
    mpu_drone.update();    

    sent_data.anglex = mpu_drone.AngleX();
    sent_data.angley = mpu_drone.AngleY();

    radio.write(&sent_data, sizeof(Data_to_be_sent));

    #ifdef SERIAL_DEBUG
        Serial.print(sent_data.anglex);
        Serial.print(",");
        Serial.print(sent_data.angley);
    #endif
}