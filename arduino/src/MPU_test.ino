
#include "MPU_minimal.h"

MPU_minimal mpu_drone;

void setup()
{
    mpu_drone.init();

    Serial.begin(9600);
}

void loop()
{
    mpu_drone.update();

    float accelx = mpu_drone.AccelX();
    float accely = mpu_drone.AccelY();
    float gyrox = mpu_drone.GyroX(); 
    float gyroy = mpu_drone.GyroY(); 
    float anglex = mpu_drone.AngleX();
    float angley = mpu_drone.AngleY();

    Serial.print(anglex);
    Serial.print(",");
    Serial.print(angley);    
}
