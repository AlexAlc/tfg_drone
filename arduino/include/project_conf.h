#ifndef _PROJECT_CONF_H_
#define _PROJECT_CONF_H_

//#include <SPI.h>
//#include <nRF24L01.h>
#ifdef GLOWVE_RECEIVER
    #include <Servo.h>
    #include "PID_v1.h"
#endif
#include <RF24.h>
#include "MPU_minimal.h"

#define SERIAL_DEBUG
#define OSCILOSCOPE

#define SERIAL_BAUDRATE     115200

//ESC's PWM pin
#define MOTOR_PIN_LEFT      6
#define MOTOR_PIN_RIGHT     5

#define ESC_WAIT            3000

//PID values 
#define PID_PROPORTIONAL    0.15
#define PID_INTEGRAL        0.2326
#define PID_DERIVATIVE      0.052

#define PID_OUT_LIM_LOW     -50
#define PID_OUT_LIM_UP      50

//MPU settings
// {0x68 , 0x69}
#define MPU_DIR             ((uint8_t)0x68)

//NRF24L01
#define RADIO_PIPE          ((uint64_t)0xE8E8F0F0E1LL)
#define RADIO_CE_PIN        9
#define RADIO_CSN_PIN       10
// {RF24_250KBPS , RF24_1MBPS , RF24_2MBPS}
#define RADIO_DATA_RATE     RF24_2MBPS

//User parameters
#define POWER_INITIAL       30
#define POWER_THRESHOLD     20
#define ANGLE_INITIAL_X     0
#define ANGLE_INITIAL_Y     0
#define ANGLE_INITIAL_Z     0

//Osciloscope pin
#define OSCILOSCOPE_SET     DDRD
#define OSCILOSCOPE_PORT    PORTD
#define OSCILOSCOPE_PIN     PD4

struct Data_to_be_sent {
  float anglex = 0;
  float angley = 0;
  float anglez = 0;
  byte joyY = 0;
};
Data_to_be_sent sent_data;

#endif //_PROJECT_CONF_H_